import datetime
from unittest import TestCase

from app import weekdate


class WeekDateTest(TestCase):

    def test_today(self):
        """
        Valid weekdate.today() type
        """
        date = weekdate.today()
        self.assertTrue(type(date) == datetime.date)

    def test_get_offset_date__returns_today(self):
        """
        Offset 0 returns the current week
        """
        shift_offset = 0
        date_today = datetime.date(2021, 5, 12)
        expected = date_today
        offset_date = weekdate.get_offset_date(date_today, shift_offset)
        self.assertEqual(expected, offset_date)

    def test_get_offset_date__returns_next_week(self):
        """
        Offset 1 returns today's date +7, i.e. a day next week
        """
        shift_offset = 1
        date_today = datetime.date(2021, 5, 12)
        offset_date = weekdate.get_offset_date(date_today, shift_offset)
        expected = datetime.date(2021, 5, 19)
        self.assertEqual(expected, offset_date)

    def test_get_offset_date__returns_previous_week(self):
        """
        Offset -1 returns today's date -7, i.e. a previous week day
        """
        shift_offset = -1
        date_today = datetime.date(2021, 5, 12)
        offset_date = weekdate.get_offset_date(date_today, shift_offset)
        expected = datetime.date(2021, 5, 5)
        self.assertEqual(expected, offset_date)
