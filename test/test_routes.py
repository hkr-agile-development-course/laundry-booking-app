import unittest
from flask_testing import TestCase  # noqa: F401
from app import app, db
from app.models import User, Admin, Booking
from config import TestConfig
import json
# Python 3
from urllib.parse import urlparse


class RoutesTestCase(unittest.TestCase):

    def setUp(self):
        app.config.from_object(TestConfig)
        self.app = app.test_client()
        db.create_all()
        a = Admin(username='admin', password='123', role='admin')
        db.session.add(a)

        u = User(
            username='username',
            first_name='first_name',
            last_name='last_name',
            password='12345',
            email='email@email.com',
            phone='0790848412',
            admin_username='admin',
            role='tenant'
        )
        db.session.add(u)
        db.session.commit()

    def tearDown(self):
        app.config['TESTING'] = False
        db.session.remove()
        db.drop_all()

    def test_home_page(self):
        res = self.app.get('/')
        sta = res.status_code
        self.assertEqual(sta, 200)
        self.assertEqual(res.content_type, "text/html; charset=utf-8")

        resu = self.app.get('/login')
        stat = resu.status_code
        self.assertEqual(stat, 200)
        self.assertTrue('Home' in resu.get_data(as_text=True))

        resp = self.app.post(
            '/login',
            data={
                'username': 'username',
                'passwrod': '12345'},
            follow_redirects=True)
        self.assertEqual(resp.status_code, 200)

    def test_admin_login(self):
        """Tests admin login, try access user page, and create_account."""
        r = self.app.get('/login')
        self.assertEqual(r.status_code, 200)
        self.assertTrue('Login Page' in r.get_data(as_text=True))

        res = self.app.post(
            '/login',
            data={
                'username': 'admin',
                'password': '123'
            },
            follow_redirects=True)
        self.assertEqual(res.status_code, 200)
        self.assertTrue('Admin' in res.get_data(as_text=True))

        resp_302 = self.app.get('/user/home')
        self.assertEqual(resp_302.status_code, 302)

        respo = self.app.get('/create_account')
        self.assertEqual(respo.status_code, 200)

        response = self.app.post(
            '/create_account',
            data={
                'username': 'user2',
                'password': '12345',
                'first_name': 'first_user',
                'last_name': 'last_user',
                'admin_username': 'admin',
                'email': 'user@email.com',
                'role': 'tenant',
                'phone': '0963258741'
            },
            follow_redirects=True)
        self.assertEqual(response.status_code, 200)

        rs = self.app.get('/user/home')
        self.assertEqual(rs.status_code, 302)

    def test_user_login(self):
        """Tests user login, and trying access create_account."""
        r = self.app.get('/login')
        self.assertEqual(r.status_code, 200)
        self.assertTrue('Login Page' in r.get_data(as_text=True))

        res = self.app.post(
            '/login',
            data={
                'username': 'username',
                'password': '12345'
            },
            follow_redirects=True
        )
        self.assertEqual(res.status_code, 200)

        # test if use tries reach admin pages
        res = self.app.get('/create_account')
        self.assertEqual(res.status_code, 302)

        res2 = self.app.get('/admin/home')
        self.assertEqual(res2.status_code, 302)

    def test_logout(self):
        """Tests logout method."""
        r = self.app.get('/logout')
        self.assertEqual(r.status_code, 302)

    def test_404page(self):
        """Tests method 404."""
        r = self.app.get('/404')
        self.assertEqual(r.status_code, 200)

    def test_change_user_password(self):
        """Tests method for change password."""
        res = self.app.post(
            '/login',
            data={
                'username': 'username',
                'password': '12345'
            },
            follow_redirects=True
        )
        self.assertEqual(res.status_code, 200)
        r = self.app.get('/change_user_password')
        self.assertEqual(r.status_code, 200)
        r = self.app.post('/change_user_password', data={'newPassword': 'abcabc'}, follow_redirects=True)
        u = User.query.filter_by(username='username').first()
        self.assertTrue(u.password == 'abcabc')

    def test_deleteing_user_account(self):
        """Tests method for deleting user account."""
        self.app.post(
            '/login',
            data={
                'username': 'admin',
                'password': '123'
            },
            follow_redirects=True)
        r = self.app.get('/delete_tenant_account')
        self.assertEqual(r.status_code, 200)
        self.app.post(
            '/delete_tenant_account',
            data={
                'username': 'username'
            },
            follow_redirects=True
        )
        u = User.query.filter_by(username='username').first()
        self.assertTrue(u is None)

    def test_handle_post_booking(self):
        """Tests that the user can make a booking"""
        # User login
        res = self.app.post(
            '/login',
            data={
                'username': 'username',
                'password': '12345'
            },
            follow_redirects=True)
        self.assertEqual(res.status_code, 200)

        self.app.get('/booking_status')
        res = self.app.post(
            '/booking_status',
            data={
                'date': '2021/05/27',
                'slot': '1',
                'user_username': 'username'
            },
            follow_redirects=True
        )
        self.assertEqual(res.status_code, 200)
        test_booking = Booking.query.filter_by(date='2021/05/27', user_username='username')
        self.assertIsNotNone(test_booking)

    def test_environmental_footprint_html_page(self):
        self.app.post(
            '/login',
            data={
                'username': 'username',
                'password': '12345'
            },
            follow_redirects=True)
        response = self.app.get('/user/environmental_footprint')
        self.assertEqual(response.status_code, 200)

    def test_environmental_footprint_admin(self):
        self.app.post(
            '/login',
            data={
                'username': 'admin',
                'password': '123'
            },
            follow_redirects=True)
        response = self.app.get('/user/environmental_footprint')
        self.assertEqual(response.status_code, 302)
        self.assertEqual(urlparse(response.location).path, '/404')

    def test_environmental_footprint_route_json_api(self):
        """Test method for environmental footprint page."""
        self.app.post(
            '/login',
            data={
                'username': 'username',
                'password': '12345'
            },
            follow_redirects=True)
        response = self.app.get("/user/environmental_footprint?year=2020")
        self.assertEqual(response.status_code, 200)
        assert response.content_type == 'application/json'
        data = json.loads(response.get_data(as_text=True))
        self.assertEqual(data['February'], 0)
