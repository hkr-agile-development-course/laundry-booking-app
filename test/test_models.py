
import unittest
from app.models import User, Admin
from flask_testing import TestCase  # noqa: F401


class TestModelsUser(unittest.TestCase):
    """Tests app/models User class"""

    def setUp(self):
        """Set up fake object to test method within the class."""
        self.test_user = User()
        self.test_user.username = 'tester'
        self.test_user.password = 'password'
        self.test_user.first_name = 'firstName'
        self.test_user.last_name = 'lastName'
        self.test_user.phone = '9638527410'
        self.test_user.email = 'tester@email.com'
        self.test_user.roll = 'tenant'

    def tearDown(self):
        """Delete the inctence of Admin class."""
        del self.test_user

    def test_check_password(self):
        """Returns true if admin enters true password, false otherwise."""
        exp_false = self.test_user.check_password('wrong_password')
        self.assertFalse(exp_false)

        exp_true = self.test_user.check_password('password')
        self.assertTrue(exp_true)

    def test_get_id(self):
        """Returns the id of the user."""
        exp = self.test_user.get_id()
        self.assertEqual(exp, 'tester')

    def test__repr__(self):
        self.test_user.username = 'user'
        exp = self.test_user.__repr__()
        self.assertTrue('item user' == exp)


class TestModelsAdmin(unittest.TestCase):
    """Tests app/models Admin class"""

    def setUp(self):
        """Set up fake object to test method within the class."""
        self.test_user = Admin()
        self.test_user.username = 'tester'
        self.test_user.password = 'password'
        self.test_user.roll = 'admin'

    def tearDown(self):
        """Delete the inctence of Admin class."""
        del self.test_user

    def test_check_password(self):
        """Returns true if admin enters true password, false otherwise."""
        exp_false = self.test_user.check_password('wrong_password')
        self.assertFalse(exp_false)

        exp_true = self.test_user.check_password('password')
        self.assertTrue(exp_true)

    def test_get_id(self):
        """Returns the id of the user."""
        exp = self.test_user.get_id()
        self.assertEqual(exp, 'tester')

    def test__repr__(self):
        self.test_user.username = 'admin'
        exp = self.test_user.__repr__()
        self.assertTrue('item admin' == exp)
