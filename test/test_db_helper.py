from unittest import TestCase
from app import db_helper
from app.db_helper import CustomSlotList, BsMock  # noqa: F401


class DbHelperTest(TestCase):

    @classmethod
    def setUpClass(cls):
        date_list = ['2021-05-03', '2021-05-04', '2021-05-05', '2021-05-06',
                     '2021-05-07', '2021-05-08', '2021-05-09']
        booked_slots = db_helper._get_dummy_slots()
        # Get slot data for use throughout the test class
        cls.slots_list = db_helper._fix_slots_list(date_list, booked_slots)

    def test_fix_slots_list__count(self):
        """
        Number of days are 7 even when booked days are less.
        """
        self.assertEqual(7, len(self.slots_list))

    def test_fix_slots_list__correct_values(self):
        """
        Defined dates that are extracted from db have their values retained.
        """
        # Present Slot values
        # 2021-05-07 are 'user1', None, 'user3', None, 'user5'
        slot_obj = self.slots_list[4]
        expected = '2021-05-07, user1, None, user3, None, user5'
        self.assertEqual(expected, slot_obj.to_string())

    def test_fix_slots_list__absent_set_to_None(self):
        """
        Dates defined in date_list, not present in booked slot have their
        all their slots generated to None.
        """
        # Absent Slot values
        # '2021-05-03' are None, None, None, None, None
        slot_obj = self.slots_list[0]
        expected = '2021-05-03, None, None, None, None, None'
        self.assertEqual(expected, self.to_string(slot_obj))

    @staticmethod
    def to_string(slot):
        """
        Converts a booked slot object to string for ease of testing.
        """
        return "{}, {}, {}, {}, {}, {}".format(slot._date, slot.slot1,
                                               slot.slot2, slot.slot3,
                                               slot.slot4, slot.slot5)

    def test_SlotList_get_id__returns_correct_column(self):
        """
        Uses dummy data to confirm that correct slot column is returned for
        SlotList via SlotList.get_id().
        """
        custom_list = CustomSlotList(self.slots_list)
        _list = custom_list.get_list()
        self.assertEqual(7, len(_list))

        # Validate that slot2 for all BookedSlot (dummy) data's 7 days are:
        # None, None, None, None, None, 'user2', 'user2'
        # This is because 03 - 06 (2021-05) are not nullified since they are
        # absent and 2021-05-07 = None, and 2021-05-08 = 2021-05-09 = 'user1'
        field_num = 2
        self.assertEqual(None, custom_list.get_tag(0, field_num))
        self.assertEqual(None, custom_list.get_tag(1, field_num))
        self.assertEqual('user2', custom_list.get_tag(6, field_num))
