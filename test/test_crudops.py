import unittest
from app import db
from app.models import Admin, User, Booking
from sqlalchemy import delete


class CrudOpsTest(unittest.TestCase):
    """
    Verify date, slot UNIQUE attribute on Bookings table.

    As a secondary goal, Create and Read ops is also verified for Booking, User
    and Admin tables.
    """

    @classmethod
    def setUpClass(cls):
        """
        Create test User and Admin instances, if they do not already exist.
        """
        cls.username = 'testUser'
        cls.adminname = 'testAdmin'

        # Admin
        test_admin = db.session.query(Admin) \
            .filter_by(username=cls.adminname).first()
        if not test_admin:
            test_admin = Admin(username=cls.adminname, password='123456',
                               role='admin')
            db.session.add(test_admin)

        # User
        test_user = db.session.query(User) \
            .filter_by(username=cls.username).first()
        if not test_user:
            test_user = User(username=cls.username, password='123456',
                             first_name='Joe', last_name='Bloggs',
                             email='joebloggs@example.com', phone='0123456789',
                             admin_username=test_admin.username, role='tenant')
            db.session.add(test_user)

        db.session.commit()

    def test__slot_date_uniqueness(self):
        """
        SQLAlchemy's default UNIQUE is to REPLACE old with new data.

        Confirm this behaviour on the table such that date, slot attributes are
        unique, and only 1 exists.
        """
        date = '2020-12-24'
        slot = '1'
        # Remove row associated wth date, slot candidate UNIQUE key
        self.delete_booking(date, slot)

        # Booking instance without id
        booking = Booking(bookingId=None, date=date,
                          slot=slot, user_username=self.username)

        # Add booking object with the same date, slot data, twice.
        db.session.add(booking)
        db.session.commit()
        db.session.add(booking)
        db.session.commit()

        # Get all instances in the Booking table associated with date, slot
        bookings = self.get_bookings(date, slot)
        # Validate that only one is present, i.e. UNIQUE is enforced
        self.assertEqual(1, len(bookings))

        # clean up
        self.delete_booking(date, slot)

    def get_bookings(self, date, slot):
        """
        @param date in 'YYYY-MM-DD' format
        """
        return db.session.query(Booking) \
            .filter_by(date=date, slot=slot).all()

    def delete_booking(self, date, slot):
        # clean up
        stmt = delete(Booking) \
            .where(Booking.date == date, Booking.slot == slot) \
            .execution_options(synchronize_session="fetch")
        db.session.execute(stmt)
        db.session.commit()

    @classmethod
    def tearDownClass(cls):
        stmt = delete(User).where(User.username == cls.username) \
            .execution_options(synchronize_session="fetch")
        db.session.execute(stmt)

        stmt = delete(Admin).where(Admin.username == cls.adminname) \
            .execution_options(synchronize_session="fetch")
        db.session.execute(stmt)
        db.session.commit()
        db.session.remove()


if __name__ == '__main__':
    unittest.main()
