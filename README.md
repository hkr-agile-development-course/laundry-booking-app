# Welcome to Tele-Laundry booking system!

This project followed Agile methodology, Scrum framework.

The purpose of the project is to get rid of the old booking system that is used in buildings with common laundry rooms.

The old system requires doing the booking physically by going down to the basement and use slots-board.

Using this app provides everyone's time and efforts.

The project based on web-page, designed to be controlled by an Admin who can create an account to a tenant before he get starting do his booking digitally.

## Table of Contents

- [Goals](#goals)

- [Instructions](#instructions)

- [Make](#make)

## Goals

Using this app has several dimensions to achieve SDG goals such as:

- Goal 8: Decent work and economic growth.

- Goal 9: Industry, Innovation and Infrastructure, where we are making it easier and faster to book laundry timeslots

- Goal 12: Responsible Consumption and Production. We are empowering the users by letting them be aware of their consumption of water and electricity based on the number of monthly-booking

Read more: [from here!](https://www.globalgoals.org/)

## Instructions

1. Install Docker Desktop [from here!](https://www.docker.com/products/docker-desktop)

2. Install the VS Code extension Remote - Containers [from here!](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)

3. Open VS Code and click on the arrows facing each other in the lower left corner.

4. In the menu that opens up, click on the _Open in Container_ entry.

5. Here you can surf the webapp: [Click here!](http://localhost:5000/)

   To log in as an admin:

   - username: admin

   - password: 123123

     ***

     To log in as a user:

   - username: user1

   - password: 123123

   ***

6. Here you can manage the database: [from here!](http://localhost:8080/)
   To log in as a root to the database:

   - username: root

   - password: root

NB: No Python virtual environment should be installed inside the container. Docker does that automatically.

## Make

The following command lines can be pasted in the terminal

<br>

To run unit-tests and show a report of the coverage:

```bash

make coverage

```

To run code cleaner:

```bash

make lint

```

To run code cleaner, unit-tests and print code coverage report:

```bash

make test

```

To get a visual report of the coverage:

```bash

make coverage-html

```
