var ctx = document.getElementById('myChart');
var myChart;
var months = ["January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"];


function createChart() {
    myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: months,
            datasets: [{
                label: '# of Votes',
                borderWidth: 1
            }]
        },
        options: {
            responsive: true,
            maintainAspectRatio: true,
            plugins: {
                legend: {
                    display: false
                },
                tooltip: {
                    enabled: false
                },
            },
            scales: {
                y: {
                    beginAtZero: true,
                    suggestedMax: 5,
                    title: {
                        display: true,
                        text: "# of washes",
                        font: {
                            size: 18
                        },
                    },
                    ticks:{
                        stepSize: 1
                    }
                }
            }
        }
    });
}


async function updateData(year) {
    document.getElementById("year").innerHTML = year;
    var data = await fetchData(year);
    myChart.data.datasets.forEach((dataset) => {
        dataset.data = data;
    });
    colorBars();
    myChart.update();
}


document.querySelectorAll('.arrows').forEach(item => {
    item.addEventListener('click', async function(){
        current_year = parseInt(document.getElementById('year').textContent)
        if (this.id == "previousYear") {
            current_year--;
        }
        else if (this.id == "nextYear") {
            current_year++;
        }
        updateData(current_year);
    });
})


function colorBars() {
    myChart.data.datasets[0]["backgroundColor"] = [];
    var backgroundColors = myChart.data.datasets[0].backgroundColor;
    myChart.data.datasets[0]["borderColor"] = [];
    var borderColors = myChart.data.datasets[0].borderColor;
    var data = myChart.data.datasets[0].data;
    for (i=0; i<=11; i++) {
        if (Object.values(data)[i] <= 2) {
            // green
            backgroundColors[i] = "rgba(75, 192, 192, 0.2)";
            borderColors[i] = "rgba(75, 192, 192, 1)";
        }
        else if (Object.values(data)[i] <= 4) {
            // orange
            backgroundColors[i] = "rgba(255, 159, 64, 0.2)";
            borderColors[i] = "rgba(255, 159, 64, 1)";
        }
        else {
            // red
            backgroundColors[i] = "rgba(255, 99, 132, 0.2)";
            borderColors[i] = "rgba(255, 99, 132, 1)";
        }
    }
}


async function fetchData(year) {
    var url = "";
    const pageURL = window.location.href;
    url = pageURL + "?year=" + year;
    console.log("REST API endpoint: " + url)

    let response = await fetch(url)
    .then(
        function(response) {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' + response.status);
            return;
        }

        return response;
        }
    )
    
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    });

    let JSONdata = await response.json();
    var data = [];
    months.forEach(month => data.push(JSONdata[month]));
    return data;

}


window.onload = async () => {
    actual_year = new Date().getFullYear();
    createChart();
    updateData(actual_year);
}
