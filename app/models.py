from app import db, login_manager
from flask_login import UserMixin
from sqlalchemy import UniqueConstraint
import enum


@login_manager.user_loader
def load_user(username):
    user = User.query.get(username)
    if user:
        return user
    return Admin.query.get(username)


class User(db.Model, UserMixin):
    username = db.Column(
        db.String(length=45), nullable=False, primary_key=True)
    password = db.Column(
        db.String(length=60), nullable=False)
    first_name = db.Column(
        db.String(length=45), nullable=False)
    last_name = db.Column(
        db.String(length=45), nullable=False)
    email = db.Column(
        db.String(length=255), nullable=False, unique=True)
    phone = db.Column(
        db.String(length=10), nullable=False, unique=True)
    admin_username = db.Column(
        db.String(length=45), db.ForeignKey('admin.username'))
    role = db.Column(db.String(length=45))

    def check_password(self, password):
        return (self.password == password)

    def get_id(self):
        return (self.username)

    def __repr__(self):
        return f'item {self.username}'


class Admin(db.Model, UserMixin):
    username = db.Column(
        db.String(length=45), nullable=False, primary_key=True)
    password = db.Column(db.String(length=60), nullable=False)
    role = db.Column(db.String(length=45))

    def get_id(self):
        return (self.username)

    def check_password(self, password):
        return (self.password == password)

    def __repr__(self):
        return f'item {self.username}'


class BookedSlots(db.Model):
    __tablename__ = 'bookedslots'

    _date = db.Column(db.Date, nullable=False, primary_key=True)
    slot1 = db.Column(db.String(length=45), nullable=False)
    slot2 = db.Column(db.String(length=45), nullable=False)
    slot3 = db.Column(db.String(length=45), nullable=False)
    slot4 = db.Column(db.String(length=45), nullable=False)
    slot5 = db.Column(db.String(length=45), nullable=False)

    def to_string(self):
        return "{}, {}, {}, {}, {}, {}".format(self._date, self.slot1,
                                               self.slot2, self.slot3,
                                               self.slot4, self.slot5)

class SlotEnum(enum.Enum):
    one = '1'
    two = '2'
    three = '3'
    four = '4'
    five = '5'


class Booking(db.Model):
    __tablename__ = 'booking'

    bookingId = db.Column(db.Integer, nullable=False, autoincrement=True)
    date = db.Column(db.Date)
    slot = db.Column(db.String(length=1), nullable=False)
    user_username = db.Column(db.String(length=45),
                              db.ForeignKey('user.username'), index=True,
                              primary_key=True)
    # UniqueConstraint(date, slot, 'uq_Date_Slot')
    date_slot_uk = UniqueConstraint(date, slot)

    def __init__(self, bookingId, user_username, slot, date):
        self.bookingId = bookingId
        self.date = date
        self.slot = slot
        self.user_username = user_username
