import datetime
from app import app, db
from flask import render_template, flash, redirect, url_for, request
from app.forms import ChangePassword, LoginForm, CreaterForm, DeleteUser
from flask_login import login_user, logout_user, login_required, current_user
from app.models import User, Admin, Booking
from app import db_helper
from app import weekdate
from app import env_footprint


@app.route('/')
@app.route('/home')
def home_page():
    """Returns the home page."""
    return render_template('home.html')


def is_admin(id, role):
    """Returns True if current_user is an admin."""
    return Admin.query.filter_by(
        username=current_user.username, role=role).first()


def is_user(id, role):
    """Returns True if current_user is a user."""
    return User.query.filter_by(
        username=current_user.username, role=role).first()


@app.route('/login', methods=['GET', 'POST'])
def login_page():
    """Return login.html

    Asserts current_user after validates the input using
    validator and db
    """
    form = LoginForm()
    if form.validate_on_submit():
        tenant = User.query.filter_by(username=form.username.data).first()
        admin = Admin.query.filter_by(username=form.username.data).first()

        if tenant and tenant.check_password(form.password.data):
            login_user(tenant)
            flash(f'Welcome {tenant.first_name}', category="success")
            return redirect(url_for('user_page'))

        elif admin and admin.check_password(form.password.data):
            login_user(admin)
            flash(f'Welcome! You are logged in as: \
            {admin.username}', category="success")
            return redirect(url_for('admin_page'))

        else:
            flash('Please check username and password!', category='danger')
    return render_template('login.html', form=form)


@app.route('/create_account', methods=['GET', 'POST'])
@login_required
def create_account():
    """Returns create_account.html

    Creates new user account after checking that fields were
    filt with valid input."""
    admin = Admin.query.filter_by(
        username=current_user.username, role=current_user.role).first()

    if admin:
        form = CreaterForm()
        if form.validate_on_submit():
            new_tenant = User(
                username=form.username.data,
                first_name=form.first_name.data,
                last_name=form.last_name.data,
                email=form.email.data,
                password=form.password1.data,
                phone=form.phone.data,
                admin_username=current_user.username,
                role='tenant'
            )

            db.session.add(new_tenant)
            db.session.commit()
            flash(f'You created a new tenant account that has Username:\
            {new_tenant.username}!!', category="success")
            return redirect(url_for('admin_page'))

        if form.errors != {}:  # for errors from the validations
            for err_msg in form.errors.values():
                flash(f'{err_msg[0]}', category='danger')

        return render_template('create_account.html', form=form)
    return redirect(url_for('error404'))


@app.route('/user/home', methods=['GET', 'POST'])
@login_required
def user_page():
    """Returns user.html."""
    if not is_user(current_user.username, current_user.role):
        return redirect(url_for('error404'))
    return handle_user_admin_POST_GET('user.html')


@app.route('/user/environmental_footprint', methods=['GET'])
@login_required
def environmental_footprint():
    """Returns environmental_footprint.html."""
    if is_admin(current_user.username, current_user.role):
        return redirect(url_for('error404'))
    year = request.args.get("year")
    if year is None:
        return handle_user_admin_POST_GET('environmental_footprint.html')
    else:
        return env_footprint.generate_json_response(year)


@app.route('/admin/home', methods=['GET', 'POST'])
@login_required
def admin_page():
    """Returns admin.html."""
    if not is_admin(current_user.username, current_user.role):
        return redirect(url_for('error404'))
    return handle_user_admin_POST_GET('admin.html')


@app.route('/logout')
def logout_page():
    """Logout current_user."""
    logout_user()
    flash('You are logged out', category='info')
    return redirect(url_for('home_page'))


@app.route('/404')
def error404():
    """Returns 404 page."""
    return render_template('error404.html')


# region Helper methods

def render_calendar(html, shift_offset: int = 0):
    """
    If 'admin.html' or 'user.html', returns a rendered html with calendar
     content infused; if calendar_div.html, the calendar div, without <html>,
     <head> and <body> tags, is returned.

    @param shift_offset with default = 0 determines the week number, when set
    to 0, this represents the current week number and its dates.
    @param html is the calender version to render
    """
    date = weekdate.get_offset_date(datetime.date.today(), shift_offset)
    week_num, date_list = weekdate.get_week_info(date)
    custom_slot_list = db_helper.get_custom_slot_list(date_list)
    return render_template(html, date_list=date_list,
                           slot_list=custom_slot_list, weeknum=week_num)


def handle_user_admin_POST_GET(home_html):
    """
    @param home_html is either 'admin.html' or 'user.html'
    """
    if request.method == 'POST':
        shift_offset = int(request.form['shift_position'])
        return render_calendar('calendar_div.html', shift_offset)
    return render_calendar(home_html)


@app.route('/delete_tenant_account', methods=['GET', 'POST'])
def delete_user():
    form = DeleteUser()
    if is_admin(current_user.username, current_user.role):
        users = User.query.all()
        if request.method == 'POST':
            if form.validate_on_submit():
                user = User.query.filter_by(username=form.username.data).first()
                if user is not None:
                    db.session.delete(user)
                    db.session.commit()
                    flash('Tenant has been deleted!', 'success')
            return redirect(url_for('delete_user'))
        return render_template('delete_user.html', form=form, users=users)
    return redirect(url_for('404'))


@app.route('/change_user_password', methods=['GET', 'POST'])
def change_password():
    form = ChangePassword()
    if is_user(current_user.username, current_user.role):
        if request.method == 'POST':
            if form.validate_on_submit():
                user = current_user
                user.password = form.newPassword.data
                db.session.add(user)
                db.session.commit()
                flash('Password has been updated!', 'success')
            elif form.errors != {}:  # for errors from the validations
                for err_msg in form.errors.values():
                    flash(f'{err_msg[0]}', category='danger')
        return render_template('change_password.html', form=form)
    return redirect(url_for('404'))


@app.route('/booking_status', methods=["POST"])
def handle_post_booking():
    """
    This route updates the booking status for the user
    """
    bookedDate = request.form.get("bookedDate")
    bookedSlot = request.form.get("bookedSlot")
    bookedTime = request.form.get("bookedTime")
    if bookedDate and bookedSlot:
        booked = Booking(None, current_user.username, bookedSlot, bookedDate)
        db.session.add(booked)
        db.session.commit()
        flash("You booked the time on " + bookedDate + ' at ' + bookedTime, 'success')
    else:
        flash("Error while booking the time", category='danger')
    return redirect(url_for('user_page'))


@app.context_processor
def utility_processor():
    def hide_button():
        disabled = ""
        if is_admin(current_user.username, current_user.role):
            disabled = "disabled"
        return disabled
    return dict(hide_button=hide_button)

# endregion
