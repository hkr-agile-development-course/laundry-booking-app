

from wtforms import StringField, PasswordField, SubmitField
from wtforms.validators import Email, DataRequired, ValidationError
from wtforms.validators import Length, EqualTo
from flask_wtf import FlaskForm
from app.models import User, Admin


class LoginForm(FlaskForm):
    """Returs True if accebtable input was written within fields."""
    username = StringField(label='User name', validators=[DataRequired()])
    password = PasswordField(label='Password', validators=[DataRequired()])
    submit = SubmitField(label='Sign In')


class CreaterForm(FlaskForm):
    """FlaskForm class run the method that starts with validate_username() first to
    check the validation.

    DataRequired(): to check that the field is not empty."""

    username = StringField(
                label='Userame:',
                validators=[Length(min=5, max=45), DataRequired()])
    first_name = StringField(
                label='Tenant firs name:',
                validators=[Length(min=4, max=45), DataRequired()])
    last_name = StringField(
                label='Tenant last name:',
                validators=[Length(min=4, max=45), DataRequired()])
    email = StringField(
                label='Email Address: ',
                validators=[Email(), Length(max=255), DataRequired()])
    password1 = PasswordField(
                label='Password: ',
                validators=[Length(min=6), DataRequired()])
    password2 = PasswordField(
                label='Confirm Password: ',
                validators=[EqualTo('password1'), DataRequired()])
    phone = PasswordField(
                label='Phonenumber: ',
                validators=[Length(max=10), DataRequired()])
    submit = SubmitField(label='Create Account')

    def validate_username(self, username):
        """Check that entered username does not
        match any other username in db."""
        user = User.query.filter_by(username=username.data).first()
        admin = Admin.query.filter_by(username=username.data).first()
        if (user or admin) is not None:
            raise ValidationError('Please use a different username.')


class DeleteUser(FlaskForm):
    username = StringField(
                label='Userame:',
                validators=[Length(min=5, max=45), DataRequired()])


class ChangePassword(FlaskForm):
    newPassword = PasswordField(
                label='Password: ',
                validators=[Length(min=6), DataRequired()])
