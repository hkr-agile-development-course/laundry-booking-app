from sqlalchemy import and_
from app import db
from app.models import BookedSlots


class BsMock:
    """
    BookedSlots Mock class for for testing purposes!
    """

    def __init__(self, _date, slot1, slot2, slot3, slot4, slot5):
        """
        @param _date is in ISO format, i.e., YYYY-MM-DD.
         """
        self._date = _date
        self.slot1 = slot1
        self.slot2 = slot2
        self.slot3 = slot3
        self.slot4 = slot4
        self.slot5 = slot5

    def to_string(self):
        return "{}, {}, {}, {}, {}, {}".format(self._date, self.slot1,
                                               self.slot2, self.slot3,
                                               self.slot4, self.slot5)


class CustomSlotList:
    """
    Class to help keep HTML template DRY. It provides iterable-like access
    to BookedSlot fields via methods with indices and field number.
    """
    def __init__(self, _list: list):
        self._list = _list

    def get_item(self, index: int):
        """
        Returns a BookedSlot object that matches the index in the _list.
        """
        return self._list[index]

    def get_list(self):
        return self._list

    def get_tag(self, obj_index, field_num):
        """
        Tag refers to the user id that is associated with the booking.

        Note: indices and num are zero-based!

        @param obj_index refers to the position of the BookedSlot object with
        the list, in ascending order in terms of their dates, where, if 0
        corresponds to 2021-05-03, then 6 corresponds to 2021-05-09.
        @param field_num corresponds to the slot field value to return
        for the given BookedSlot object.
        """
        day_slots = self._list[obj_index]
        if field_num == 0:
            return day_slots._date
        elif field_num == 1:
            return day_slots.slot1
        elif field_num == 2:
            return day_slots.slot2
        elif field_num == 3:
            return day_slots.slot3
        elif field_num == 4:
            return day_slots.slot4
        elif field_num == 5:
            return day_slots.slot5
        raise IndexError("{} is an invalid method_index.".format(field_num))


def _get_dummy_slots():
    """
    Returns a list of Slot objects.
    """
    return [BsMock('2021-05-07', 'user1', None, 'user3', None, 'user5'),
            BsMock('2021-05-08', 'user1', 'user2', 'user3', 'user4', 'user5'),
            BsMock('2021-05-09', 'user1', 'user2', 'user3', 'user4', 'user5'),
            BsMock('2021-05-10', 'user1', 'user2', 'user3', 'user4', 'user5'),
            BsMock('2021-05-11', 'user1', 'user2', 'user3', 'user4', 'user5'),
            BsMock('2021-05-12', 'user1', 'user2', 'user3', 'user4', 'user5'),
            BsMock('2021-05-13', 'user1', 'user2', 'user3', 'user4', 'user5')]


def _fix_slots_list(date_list, booked_slots) -> list:
    """
    Manages booked slot population for html template calendar, in particular
    it generates empty slots for omitted dates.
    """
    slots_list = []
    for date in date_list:
        # Prepare an empty object in case date does not exist in booked_slots
        slot = BookedSlots()
        slot._date = date
        for day_slot in booked_slots:
            if date == str(day_slot._date):
                slot = day_slot
                break
        slots_list.append(slot)
    return slots_list


def get_custom_slot_list(date_list: list) -> CustomSlotList:
    """
    Returns a list of BookedSlots objects per week, from the start and end
    dates (inclusive).
    """
    start_date = date_list[0]
    end_date = date_list[6]
    session = db.session
    booked_slots = session.query(BookedSlots).filter(
        and_(BookedSlots._date >= start_date, BookedSlots._date <= end_date))
    slot_list = _fix_slots_list(date_list, booked_slots)
    return CustomSlotList(slot_list)
