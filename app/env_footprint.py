from app.models import BookedSlots
from sqlalchemy import extract
from enum import Enum
from flask import jsonify
from flask_login import current_user


class Month(Enum):
    January = 1
    February = 2
    March = 3
    April = 4
    May = 5
    June = 6
    July = 7
    August = 8
    September = 9
    October = 10
    November = 11
    December = 12


def generate_json_response(year):
    monthly_washing = {}
    year_result = BookedSlots.query.filter(extract("year", BookedSlots._date) == year)
    for month in range(1, 13):
        month_result = year_result.filter(extract("month", BookedSlots._date) == month).all()
        washing_this_month = 0
        for booked_day in month_result:
            for slot in range(1, 6):
                slot_name = "slot" + str(slot)
                user = booked_day.__dict__[slot_name]
                if user == current_user.username:
                    washing_this_month += 1
        monthly_washing[Month(month).name] = washing_this_month
    return jsonify(monthly_washing)
