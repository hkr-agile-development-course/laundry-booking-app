from flask import Flask
from flask_login import LoginManager
from flask_mysqldb import MySQL, MySQLdb   # noqa: F401
from flask_sqlalchemy import SQLAlchemy
from config import Config

app = Flask(__name__)
app.config.from_object(Config)
app.jinja_env.filters['zip'] = zip

db = SQLAlchemy(app)

login_manager = LoginManager(app)
login_manager.login_view = 'login'

from app import routes, db_helper  # noqa: F401, E402
