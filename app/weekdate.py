import datetime
from datetime import date


def today():
    return date.today()


def get_week_info(now: datetime.date):
    """
    Returns the current ISO week number and a list of dates belonging to it,
    starting from Monday to Sunday.

    @param now is the date with which the accompanying week days of the
    week should be modelled on.
    """
    # dt.weekday(dt.today()) returns 0 ... 6 -> Monday to Sunday
    offset = float(24 * date.weekday(date.today()))
    # In subtracting the delta by now, the start of the week, Monday is found
    start_date = (now - datetime.timedelta(hours=offset))
    date_list = list()
    # Offset set to 0, use in loop to generate the rest of the week,
    # by incrementing each iteration by 24 hours.
    offset = 0
    for i in range(7):
        date_str = (start_date + datetime.timedelta(hours=offset))\
            .strftime('%Y-%m-%d')
        date_list.append(date_str)
        offset += 24
    return now.isocalendar()[1], date_list


def get_offset_date(now: datetime.date, shift_offset: int = 0):
    offset_hrs = (7 * 24) * shift_offset
    return now + datetime.timedelta(hours=offset_hrs)
