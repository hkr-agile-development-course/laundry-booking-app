clean:
	rm -f test/.coverage *.pyc
	rm -rf test/__pycache__
	rm -rf htmlcov
	rm -rf doc
	rm -rf app/test_db.db

coverage:
	coverage run -m unittest
	coverage report -m
	rm -rf app/test_db.db

coverage-html: coverage
	coverage html

coverage-xml: coverage
	coverage xml

test:  lint coverage

lint:
	flake8