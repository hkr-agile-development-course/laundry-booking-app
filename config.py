
class Config(object):
    TESTING = False
    SQLALCHEMY_DATABASE_URI = 'mysql+pymysql://root:root@db:3306/laundry_system'
    SECRET_KEY = 'you-will-never-guess'
    SQLALCHEMY_TRACK_MODIFICATIONS = False
    WTF_CSRF_ENABLED = True
    MYSQL_CURSORCLASS = 'DictCursor'


class TestConfig(Config):
    DEBUG = True
    TESTING = True
    WTF_CSRF_ENABLED = False
    SQLALCHEMY_DATABASE_URI = 'sqlite:///test_db.db'
