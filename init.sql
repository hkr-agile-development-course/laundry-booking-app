-- MySQL dump 10.13  Distrib 8.0.24, for Win64 (x86_64)
--
-- Host: localhost    Database: laundry_system
-- ------------------------------------------------------
-- Server version	8.0.24

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `admin`
--

USE laundry_system;

DROP TABLE IF EXISTS `admin`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `admin` (
  `username` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `admin`
--

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;
INSERT INTO `admin` VALUES ('admin','123123','admin');
/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Temporary view structure for view `bookedslots`
--

DROP TABLE IF EXISTS `bookedslots`;
/*!50001 DROP VIEW IF EXISTS `bookedslots`*/;
SET @saved_cs_client     = @@character_set_client;
/*!50503 SET character_set_client = utf8mb4 */;
/*!50001 CREATE VIEW `bookedslots` AS SELECT
 1 AS `_date`,
 1 AS `slot1`,
 1 AS `slot2`,
 1 AS `slot3`,
 1 AS `slot4`,
 1 AS `slot5`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `booking`
--

DROP TABLE IF EXISTS `booking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `booking` (
  `bookingId` int NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `slot` ENUM('1', '2', '3', '4', '5') NOT NULL,
  `user_username` varchar(45) NOT NULL,
  PRIMARY KEY (`bookingId`,`user_username`),
  UNIQUE KEY `date_slot_uk` (`date`,`slot`),
  KEY `fk_MyBooking_user1_idx` (`user_username`),
  CONSTRAINT `fk_MyBooking_user1` FOREIGN KEY (`user_username`) REFERENCES `user` (`username`) ON DELETE CASCADE 
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `booking`
--

LOCK TABLES `booking` WRITE;
/*!40000 ALTER TABLE `booking` DISABLE KEYS */;
INSERT INTO `booking` VALUES 
(3,'2021-05-07','1','user1'),
(4,'2021-05-08','1','user1'),
(5,'2021-05-07','2','user2'),
(6,'2021-05-08','5','user3'),
(7,'2021-05-10','3','user1'),
(8,'2021-04-07','4','user1'),
(9,'2021-03-08','4','user1'),
(10,'2021-02-07','2','user1'),
(11,'2021-02-08','5','user1'),
(12,'2021-02-10','3','user1'),
(13,'2021-01-17','2','user1'),
(14,'2021-01-28','5','user1'),
(15,'2021-05-20','3','user1'),
(16,'2021-05-01','2','user1'),
(17,'2021-02-13','5','user1'),
(18,'2021-02-18','3','user1'),
(19,'2021-03-27','2','user1'),
(20,'2021-01-02','1','user1'),
(21,'2021-04-11','3','user1'),
(22,'2020-08-02','5','user1'),
(23,'2020-08-11','4','user1'),
(24,'2020-07-02','1','user1'),
(25,'2020-05-11','3','user1'),
(26,'2020-01-04','5','user1'),
(27,'2020-03-11','2','user1'),
(28,'2020-01-02','1','user1'),
(29,'2020-04-11','3','user1');
/*!40000 ALTER TABLE `booking` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `user` (
  `username` varchar(45) NOT NULL,
  `first_name` varchar(45) NOT NULL,
  `last_name` varchar(45) NOT NULL,
  `password` varchar(45) NOT NULL,
  `phone` varchar(10) NOT NULL,
  `email` varchar(255) NOT NULL,
  `admin_username` varchar(45) NOT NULL,
  `role` varchar(45) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `fk_user_admin_idx` (`admin_username`),
  CONSTRAINT `fk_user_admin` FOREIGN KEY (`admin_username`) REFERENCES `admin` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb3;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES ('user1','user','user','123123','0741258963','email@email.com','admin','tenant'),('user2','Anna','Brooks','password','1111111111','anna@brooks.com','admin','tenant'),('user3','Ben','Marcbet','password','1234567891','ben@marc.com','admin','tenant');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Final view structure for view `bookedslots`
--

/*!50001 DROP VIEW IF EXISTS `bookedslots`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8mb4 */;
/*!50001 SET character_set_results     = utf8mb4 */;
/*!50001 SET collation_connection      = utf8mb4_0900_ai_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `bookedslots` AS select distinct `booking`.`date` AS `_date`,(select `booking`.`user_username` from `booking` where ((`booking`.`date` = `_date`) and (`booking`.`slot` = '1'))) AS `slot1`,(select `booking`.`user_username` from `booking` where ((`booking`.`date` = `_date`) and (`booking`.`slot` = '2'))) AS `slot2`,(select `booking`.`user_username` from `booking` where ((`booking`.`date` = `_date`) and (`booking`.`slot` = '3'))) AS `slot3`,(select `booking`.`user_username` from `booking` where ((`booking`.`date` = `_date`) and (`booking`.`slot` = '4'))) AS `slot4`,(select `booking`.`user_username` from `booking` where ((`booking`.`date` = `_date`) and (`booking`.`slot` = '5'))) AS `slot5` from `booking` where `booking`.`date` in (select distinct `booking`.`date` from `booking`) */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-05-08 15:19:08
